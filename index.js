var fs = require('fs');
var express = require('express');
var express_session = require('express-session');
var bodyParser = require('body-parser');
var requireDir = require('require-dir');

module.exports = function(config){

    GLOBAL._ = require('underscore');
    
    GLOBAL.fmvc = {};

    GLOBAL.fmvc.extendClass = function(child, father){
        child.prototype = father.prototype;
        child.prototype.constructor = child;
    };
    GLOBAL.fmvc.inherits = require('util').inherits;

    GLOBAL.fmvc.config = config;
    GLOBAL.fmvc.Model = require(__dirname+'/components/Model.js');
    GLOBAL.fmvc.Controller = require(__dirname+'/components/Controller.js');
    GLOBAL.fmvc.UserIdentity = require(__dirname+'/components/UserIdentity.js');


    requireDir(__dirname+'/components/helpers');

    var app = express();
    //Sessioni sul DB
    //Anti CSRF
    //socket.io
    app.use(express_session({
        secret: config.sessionSecret,
        resave: true,
        saveUninitialized: true
    }));

    app.use(bodyParser.urlencoded({ extended: false }));

    var controllers = requireDir(fmvc.config.applicationDirectory+'/controllers');
    
    //Each controller
    Object.keys(controllers).forEach(function(cname){
        
        controllers[cname].name = cname.replace(/Controller/, "").toLowerCase();
        function CurrentController(cd){

            CurrentController.super_.apply(this, [cd]);
        }
        fmvc.inherits(CurrentController, fmvc.Controller);
        CurrentController.prototype = _.extend_deep(CurrentController.prototype, controllers[cname]);
        
        var controllerName = CurrentController.prototype.getControllerName();

        //Each action
        Object.keys(CurrentController.prototype.actions).forEach(function(aname){
        
            app.all('/'+controllerName+'/'+aname, function(req, res){
                var cc = new CurrentController({
                    req: req,
                    res: res,
                    appDir: fmvc.config.applicationDirectory
                });
                cc.doAction.bind(cc)(aname);
            });
        
        });

        //Default render index action
        app.all('/'+controllerName+'/', function(req, res){

            var cc = new CurrentController({
                req: req,
                res: res,
                appDir: fmvc.config.applicationDirectory
            });
            cc.doAction.bind(cc)('index');
        });

        //Redirect without '/'
        app.all('/'+controllerName, function(req, res){
            res.redirect('/'+controllerName+'/');
        });

    });

    app.listen(fmvc.config.port);

};

var fs = require('fs');
var UserIdentity = require(__dirname+'/UserIdentity.js');

function Controller(cd){
    this.req = cd.req;
    this.res = cd.res;
    this.appDir = cd.appDir;
    
}



Controller.prototype.getControllerName = function(){
    return this.name;
    //return this.constructor.name.replace(/Controller/, "").toLowerCase();
};

Controller.prototype.getUserIdentity = function(){
    return this.req.session.userIdentity;
};

Controller.prototype.isGuest = function(){
    return this.req.session.userIdentity ? false : true;
};

Controller.prototype.hasPOST = function(){
    return Object.getOwnPropertyNames(this.req.body).length;
};

Controller.prototype.hasGET = function(){
    return Object.getOwnPropertyNames(this.req.query).length;
};




Controller.prototype.doLogout = function(){
    this.req.session.destroy();
};

Controller.prototype.doLogin = function(userIdentity){
    if(userIdentity instanceof UserIdentity)
        this.req.session.userIdentity = userIdentity;
};


/*
    Uniforms the view name, it prepends the controller's name where it's not supplied.
    Obviously the controller's name is the name of the controller where the method is called.

    viewName [string]: The name of the view to be eventually prepended with the controller's name string.
    
*/
Controller.prototype.uniformViewName = function(viewName){
    var sViewName = viewName.split('/');
    if(sViewName && sViewName.length == '1')
    {
        viewName = this.getControllerName()+'/'+sViewName[0];
    }
    return viewName;
};

/*
    Does the same things uniformViewName does.
*/
Controller.prototype.uniformActionName = function(actionName){
    return this.uniformViewName(actionName);
};


/*
    Just renders a view using res.send and the templating engine.

    viewName [string]: The name of the view to be rendered. The view must be into the "views" folder.
        Example:
            "index": renders the "index" view in your current controller directory, if it's called from the "admin" controller it would render the "admin/index" view.
            "user/index": renders the "index" view in the "user" controller directory, even if you are in the "guest" controller.

    ctx [object]: The context to be passed to the template engine.
    
    partial [boolean]: Defines wether or not to render the view inside the layout in "applicationDirectory/views/layout/main.ejs".

*/
Controller.prototype.render = function(viewName, ctx, partial){

    viewName = this.uniformViewName(viewName);
    ctx.controller = this;

    var viewTemplate = fs.readFileSync(this.appDir+'/views/'+viewName+'.html').toString();
    var cViewTemplate = _.template(viewTemplate);

    if(partial){
        this.res.send(cViewTemplate(ctx));
    }
    else{
        var layoutTemplate = fs.readFileSync(this.appDir+'/views/layout/main.html').toString();
        var cLayoutTemplate = _.template(layoutTemplate);
        this.res.send(cLayoutTemplate({body: cViewTemplate(ctx)}));
    }
};


Controller.prototype.redirect = function(where){

    this.res.redirect('/'+this.uniformActionName(where));

};


/*
    Executes a given action

    action [string]: the action in the controller to be executed.
        Example:
            "index": calls this.actions.index().

*/
Controller.prototype.doAction = function(action){
    
    this.beforeAction();
    this.actions[action].bind(this)();
    this.afterAction();
};

Controller.prototype.beforeAction = function(){

};

Controller.prototype.afterAction = function(){
};


Controller.prototype.events = {};


Controller.prototype.actions = {};

Controller.prototype.actions.index = function(){
    this.res.send('default index action')
};
module.exports = Controller;